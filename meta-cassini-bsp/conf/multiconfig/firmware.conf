# Based on: meta-poky/conf/distro/poky-tiny.conf
# In open-source project: https://git.yoctoproject.org/poky/tree/meta-poky/conf/distro/poky-tiny.conf?id=eaf8ce9d39a2c0d9c42b32fb6596ab4302f93a1a
#
# Original file: Copyright (c) 2011, Intel Corporation.
# Modifications: Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

TCLIBC = "musl"

FULL_OPTIMIZATION="-Os -pipe ${DEBUG_FLAGS}"

# FIXME: consider adding a new "tiny" feature
#DISTRO_FEATURES_append = " tiny"

# Distro config is evaluated after the machine config, so we have to explicitly
# set the kernel provider to override a machine config.
PREFERRED_PROVIDER_virtual/kernel = "linux-yocto-tiny"
PREFERRED_VERSION_linux-yocto-tiny ?= "5.15%"

# We can use packagegroup-core-boot, but in the future we may need a new packagegroup-core-tiny
#POKY_DEFAULT_EXTRA_RDEPENDS += "packagegroup-core-boot"
# Drop kernel-module-af-packet from RRECOMMENDS
POKY_DEFAULT_EXTRA_RRECOMMENDS = ""

# FIXME: what should we do with this?
TCLIBCAPPEND = ""

# Disable wide char support for ncurses as we don't include it in
# in the LIBC features below.
# Leave native enable to avoid build failures
ENABLE_WIDEC = "false"
ENABLE_WIDEC:class-native = "true"

# Drop native language support. This removes the
# eglibc->bash->gettext->l ibc-posix-clang-wchar dependency.
USE_NLS="no"

# Comment out any of the lines below to disable them in the build
# DISTRO_FEATURES options:
# alsa bluetooth ext2 pcmcia usbgadget usbhost wifi nfs zeroconf pci
DISTRO_FEATURES_TINY = "pci"
DISTRO_FEATURES_NET = "ipv4 ipv6"
DISTRO_FEATURES_USB = "usbhost"
#DISTRO_FEATURES_USBGADGET = "usbgadget"
#DISTRO_FEATURES_WIFI = "wifi"

DISTRO_FEATURES = "${DISTRO_FEATURES_TINY} \
                   ${DISTRO_FEATURES_NET} \
                   ${DISTRO_FEATURES_USB} \
                   ${DISTRO_FEATURES_USBGADGET} \
                   ${DISTRO_FEATURES_WIFI} \
                  "

# Enable LFS - see bug YOCTO #5865
DISTRO_FEATURES:append:libc-musl = " largefile"

DISTRO_FEATURES:class-native = "${DISTRO_FEATURES_DEFAULT} ${POKY_DEFAULT_DISTRO_FEATURES}"
DISTRO_FEATURES:class-nativesdk = "${DISTRO_FEATURES_DEFAULT} ${POKY_DEFAULT_DISTRO_FEATURES}"

# enable mdev/busybox for init
INIT_MANAGER = "mdev-busybox"
VIRTUAL-RUNTIME_dev_manager = "busybox-mdev"
VIRTUAL-RUNTIME_init_manager = "busybox"
VIRTUAL-RUNTIME_initscripts = "initscripts"
VIRTUAL-RUNTIME_keymaps = "keymaps"
VIRTUAL-RUNTIME_login_manager = "busybox"

# FIXME: Consider adding "modules" to MACHINE_FEATURES and using that in
# packagegroup-core-base to select modutils-initscripts or not.  Similar with "net" and
# netbase.

# By default we only support initramfs. We don't build live as that
# pulls in a lot of dependencies for the live image and the installer, like
# udev, grub, etc.  These pull in gettext, which fails to build with wide
# character support.
IMAGE_FSTYPES = "cpio.gz"
QB_DEFAULT_FSTYPE = "cpio.gz"

# Drop v86d from qemu dependency list (we support serial)
# Drop grub from meta-intel BSPs
# FIXME: A different mechanism is needed here. We could define -tiny
#        variants of all compatible machines, but that leads to a lot
#        more machine configs to maintain long term.
MACHINE_ESSENTIAL_EXTRA_RDEPENDS = ""

# The mtrace script included by eglibc is a perl script. This means the system
# will build perl in case this package is installed. Since we don't care about
# this script for the purposes of tiny, remove the dependency from here.
RDEPENDS:${PN}-mtrace:pn-eglibc = ""

SKIP_RECIPE[build-appliance-image] = "not buildable with poky-tiny"
SKIP_RECIPE[core-image-rt] = "not buildable with poky-tiny"
SKIP_RECIPE[core-image-rt-sdk] = "not buildable with poky-tiny"
SKIP_RECIPE[core-image-sato] = "not buildable with poky-tiny"
SKIP_RECIPE[core-image-sato-dev] = "not buildable with poky-tiny"
SKIP_RECIPE[core-image-sato-sdk] = "not buildable with poky-tiny"
SKIP_RECIPE[core-image-x11] = "not buildable with poky-tiny"
SKIP_RECIPE[core-image-weston] = "not buildable with poky-tiny"

# Disable python usage in opkg-utils since it won't build with tiny config
PACKAGECONFIG:remove:pn-opkg-utils = "python"

DISTRO_FEATURES:remove = "pci ipv6 usbhost systemd virtualization seccomp k3s cassini-parsec cassini-security"

# Cassini specific config options below
# Ensure this config has it's own TMPDIR
TMPDIR:append = "-firmware"

KERNEL_CLASSES:remove = "containers_kernelcfg_check k3s_kernelcfg_check"

# Include any machine specific extras
include conf/machine/include/${MACHINE}-firmware-extra-settings.inc
