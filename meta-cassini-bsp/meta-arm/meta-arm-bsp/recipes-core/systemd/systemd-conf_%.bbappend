# Copyright (c) 2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${THISDIR}:"

SRC_URI:append:corstone1000 = "\
    file://system.conf \
"

do_install:append:corstone1000() {
	install -D -m0644 ${WORKDIR}/system.conf ${D}${systemd_system_unitdir}.conf.d/01-${PN}.conf
}
