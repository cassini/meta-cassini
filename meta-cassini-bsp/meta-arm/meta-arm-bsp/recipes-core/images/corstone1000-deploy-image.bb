# Copyright (c) 2022-2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Firmware image deploying multiconfig firmware"

inherit deploy nopackages

LICENSE = "MIT"
PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(corstone1000-mps3|corstone1000-fvp)"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_install[noexec] = "1"

FIRMWARE_BINARIES = "corstone1000-image-${MACHINE}.wic \
                     bl1.bin \
                     es_flashfw.bin \
                     ${@bb.utils.contains('DISTRO_FEATURES','cassini-test', \
                     'corstone1000-utils-overlay-image-${MACHINE}.tar.bz2', '', d)} \
                    "

do_deploy() {
    firmwareloc="${TOPDIR}/tmp-firmware/deploy/images/${MACHINE}"
    for firmware in ${FIRMWARE_BINARIES}; do
        cp -av ${firmwareloc}/${firmware} ${DEPLOYDIR}/
        if [ -L ${firmwareloc}/${firmware} ]; then
            cp -av ${firmwareloc}/$(readlink ${firmwareloc}/${firmware}) ${DEPLOYDIR}/
        fi
    done
}

do_deploy[umask] = "022"
do_deploy[mcdepends] = "mc::firmware:corstone1000-image:do_image_complete \
                        ${@bb.utils.contains('DISTRO_FEATURES', 'cassini-test', \
                        'mc::firmware:corstone1000-utils-overlay-image:do_image_complete', '', d)} \
                       "

addtask deploy after do_prepare_recipe_sysroot
