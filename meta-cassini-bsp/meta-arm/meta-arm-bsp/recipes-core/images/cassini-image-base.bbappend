# Copyright (c) 2023 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

# Corstone-1000's FVP needs images to have a extra empty space at the end or the FVP
# truncates the MMC card resulting in invalid GPT entries as the GPT last block is
# beyond the MMC cards reported size
IMAGE_TYPES += "wic.pad"

CONVERSIONTYPES += "pad"

CONVERSION_CMD:pad = "cp ${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${type} ${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${type}.pad && dd if=/dev/zero count=1024 bs=512 >> ${IMAGE_NAME}${IMAGE_NAME_SUFFIX}.${type}.pad"

IMAGE_FSTYPES:append:corstone1000-fvp = " wic.pad wic.pad.gz"

# We do not want to build these machine added extra's in the main cassini image context as
# they should only be built in firmware context
# This list needs to be kept in sync with the list added in
# layers/meta-arm/meta-arm-bsp/conf/machine/include/corstone1000.inc
EXTRA_IMAGEDEPENDS:remove:corstone1000 = "trusted-firmware-a virtual/trusted-firmware-m u-boot optee-os external-system"

# Ensure cassini-image-* also builds the firmware for corstone1000 using a different libc
do_image_complete[depends] = "${@bb.utils.contains_any('MACHINE','corstone1000-mps3 corstone1000-fvp', \
                              'corstone1000-deploy-image:do_deploy', '', d)}"
