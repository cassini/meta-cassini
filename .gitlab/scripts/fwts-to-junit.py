#!/usr/bin/env python3
# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

"""FWTS to Junit logfie converter

This script converts the logfile for the Firmware Test Suite and converts
it into the test result file in JUnit format
"""

import argparse
import re

from junit_xml import TestSuite, TestCase


def main():
    """Parses command line arguments and calls parseData function

    Parameters
    ----------

    Returns
    -------
    """

    parser = argparse.ArgumentParser(
        description='Converts FWTS test log into junit format')

    parser.add_argument("log_file", help="FWTS test log file")
    parser.add_argument("output_file", help="Output Junit log file")

    args = parser.parse_args()

    parseData(args.log_file, args.output_file)


def parseData(log_file_path, export_file):
    """Parses the input file and generates the output

    Parameters
    ----------
    log_file_path : str
        The location of the log file to be parsed
    export_file : str
        The location of the junit output file to be created

    Returns
    -------
    """

    test_cases = []

    with open(log_file_path, "r") as file:
        result_log = file.read()

        test_class_found = True
        while test_class_found:
            test_class_found = re.match(
                r'(.*?)^(\w+?):\s([^\n]+?\.$)\n^-{80}$(.*?)^={80}$(.*)',
                result_log,
                re.MULTILINE | re.DOTALL)
            if test_class_found:
                result_log = test_class_found.group(5)
                test_cases += parse_test_case(
                    test_class_found.group(2).replace('\n', ' '),
                    test_class_found.group(3),
                    test_class_found.group(4))

    file.close()

    ts = TestSuite("FWTS test suite", test_cases)

    with open(export_file, 'w') as f:
        TestSuite.to_file(f, [ts])


def parse_test_case(case_name, case_description, log_section):
    """Parses the data of one test case

    Parameters
    ----------
    case_name : str
        The name of the test case
    case_description : str
        Description of the test case
    log_section : str
        Section of the logfile relevant to the test case

    Returns
    -------
    list
        a list of the found tests in the test case log section
    """

    tests = []

    # Now remove first form of aborted test
    aborted_test_found = True
    while aborted_test_found:
        aborted_test_found = re.match(
            r'(.*?)^([\w\s]+)\.\sAborted\.$(.*)',
            log_section,
            re.MULTILINE | re.DOTALL)
        if aborted_test_found:
            log_section = aborted_test_found.group(
                1) + aborted_test_found.group(3)

            test_case = TestCase(
                aborted_test_found.group(2), case_name, 0, "", "")
            test_case.add_error_info("Aborted")
            tests.append(test_case)

    # Now remove second form of aborted test
    aborted_test_found = True
    while aborted_test_found:
        aborted_test_found = re.match(
            r'(.*?)^Aborted\stest,\s([\w\s]+)\.$(.*)',
            log_section,
            re.MULTILINE | re.DOTALL)
        if aborted_test_found:
            log_section = aborted_test_found.group(
                1) + aborted_test_found.group(3)

            test_case = TestCase(
                aborted_test_found.group(2), case_name, 0, "", "")
            test_case.add_error_info("Aborted")
            tests.append(test_case)

    # Now find each test
    test_found = True
    while test_found:
        test_found = re.match(r'.*?^Test\s(\d+)\sof\s(\d+):\s(.+?$)'
                              r'((?:(?!^Test\s\d+\sof\s\d+:).)+)(.*)',
                              log_section,
                              re.MULTILINE | re.DOTALL)
        if test_found:
            log_section = test_found.group(5)
            tests += parse_test(case_name,
                                test_found.group(1),
                                test_found.group(2),
                                test_found.group(3).replace('\n', ' '),
                                test_found.group(4))

    return tests


def parse_test(case_name, test_number, test_total, test_name, log_section):
    """Parses the data of one test

    Parameters
    ----------
    case_name : str
        The name of the test case
    test_number : int
        This tests number
    test_total : int
        The total number of tests
    test_name : str
        The name of the test
    log_section: str
        Section of the logfile relevant to the test

    Returns
    -------
    list
        a list of the found tests steps in the test log section
    """

    tests = []

    sub_test_found = True
    sub_test_number = -1
    while sub_test_found:
        sub_test_found = re.match(
            r'(.*?)^(PASSED|SKIPPED|FAILED)(.*?):\sTest\s(\d+),\s(.+?\.)(.*)',
            log_section,
            re.MULTILINE | re.DOTALL)
        if sub_test_found:
            log_section = sub_test_found.group(6)
            sub_test_number += 1
            test_case = TestCase(test_name if sub_test_number < 1
                                 else test_name + ":" + str(sub_test_number),
                                 case_name,
                                 0,
                                 sub_test_found.group(1), "")

            if sub_test_found.group(2) == 'FAILED':
                test_case.add_failure_info(sub_test_found.group(3))
            elif sub_test_found.group(2) == 'SKIPPED':
                test_case.add_skipped_info(sub_test_found.group(5))

            tests.append(test_case)

    return tests


if __name__ == '__main__':
    main()
