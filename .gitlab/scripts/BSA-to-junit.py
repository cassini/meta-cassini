#!/usr/bin/env python3
# Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
#
# SPDX-License-Identifier: MIT

import argparse
import re

from junit_xml import TestSuite, TestCase


def main():
    parser = argparse.ArgumentParser(
        description='Converts BSA test log into junit format')

    parser.add_argument("log_file", help="BSA test log file")
    parser.add_argument("output_file", help="Output Junit log file")
    parser.add_argument("-n", "--name", action="store",
                        default="BSA test suite",
                        help="Test Suite Name to be used in the ouput file")

    args = parser.parse_args()

    parseData(args.log_file, args.output_file, args.name)


def parseData(log_file_path, export_file, name):
    test_cases = []
    with open(log_file_path, "r") as file:
        test_cases = processResultFile(file)

    ts = TestSuite(name, test_cases)

    with open(export_file, 'w') as f:
        TestSuite.to_file(f, [ts])


def processResultFile(result_file):
    test = -1
    update_lines = {}
    test_cases = []
    for line in result_file.readlines():
        clean_line = stripTimestamp(line)

        if re.match(r'^\s+Total Tests run\s+=\s+(\d+);'
                    r'\s+Tests Passed\s+=\s+(\d.+)\s+'
                    r'Tests Failed\s+=\s+(\d+)',
                    clean_line):
            # Found summary at end of log
            continue

        match = re.match(r'^\s+(\d+)\s+:(.+)', clean_line)
        if match:
            test += 1
            data = match.group(0)
            update_lines[test] = data
            if test > 0:
                # Convert previous test data into Junit test case
                test_cases.append(addResult(update_lines[test - 1]))
            continue
        if test == -1:
            # Have not found first test case yet
            continue
        update_lines[test] += clean_line

    if test > 0:
        # Convert final test data into Junit test case
        test_cases.append(addResult(update_lines[test]))

    return test_cases


def stripTimestamp(line):
    has_timestamp = re.match(r'^\[[\d\s]+\.\d+\](.*)$', line)
    clean_line = line
    if has_timestamp:
        clean_line = has_timestamp.group(1)

    return clean_line


def addResult(log_section):
    match = re.match(
        r'^\s+(\d+)\s+:\s*([\s\S]+): Result:\s+-*(\w+)-*.*', log_section)

    test_number = match.group(1)
    test_result = match.group(3)
    test_output = match.group(2)

    test_name, test_number, test_output = findTestName(test_output,
                                                       test_number)

    test_case = TestCase(test_name, test_number, 0, test_output.strip(), "")

    if test_result == 'FAIL':
        test_case.add_failure_info(test_result)
    elif test_result == 'SKIPPED':
        test_case.add_skipped_info(test_result)

    return test_case


def findTestName(test_output, test_number):
    test_data = re.match(r'\s*((\S+):)?\s*(( ?\S+)+)\s+(.*)?', test_output)

    test_name = test_data.group(3)
    if test_data.group(2) is not None:
        test_number = test_number + " " + test_data.group(2)
    if test_data.group(5) is not None:
        test_output = test_data.group(5)
    else:
        test_output = ""

    return (test_name, test_number, test_output)


if __name__ == '__main__':
    main()
