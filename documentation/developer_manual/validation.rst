..
 # Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
 #
 # SPDX-License-Identifier: MIT

##########
Validation
##########

*************************************
Build-Time Kernel Configuration Check
*************************************

After the kernel configuration has been produced during the build, it is
checked to validate the presence of necessary kernel configuration to comply
with specific Cassini functionalities.

A list of required kernel configs is used as a reference, and compared against
the list of available configs in the kernel build. All reference configs need
to be present either as module (``=m``) or built-in (``=y``). A BitBake warning
message is produced if the kernel is not configured as expected.

The following kernel configuration checks are performed:

* **Container engine support**:

  Check performed via:
  ``meta-cassini-distro/classes/containers_kernelcfg_check.bbclass``.
  By default |Yocto Docker config|_ is used as the reference.

* **K3s orchestration support**:

  Check performed via:
  ``meta-cassini-distro/classes/k3s_kernelcfg_check.bbclass``.
  By default |Yocto K3s config|_ is used as the reference.

.. _run-time_integration_tests_label:

**************************
Run-Time Integration Tests
**************************

The ``meta-cassini-tests`` Yocto layer contains recipes and configuration for
including run-time integration tests into an Cassini distribution, to be run
manually after booting the image.

The Cassini run-time integration tests are a mechanism for validating Cassini
core functionalities. The following integration test suites are included in the
Cassini distribution image:

* `Container Engine Tests`_
* `K3s Orchestration Tests`_ (local deployment of a K3s pod)
* `User Accounts Tests`_

The tests are built as a |Yocto Package Test|_ (ptest), and implemented using
the |Bash Automated Test System|_ (BATS).

Run-time integration tests are not included in a Cassini distribution image by
default, and must instead be included explicitly. See
:ref:`build_system_run-time_integration_tests_label` within the Build System
documentation for details on how to include the tests.

The test suites are executed using the ``test`` user account, which has
``sudo`` privileges. More information about user accounts can be found at
:ref:`User Accounts<developer_manual/user_accounts:User Accounts>`.

.. _running_the_tests_label:

Running the Tests
=================

If the tests have been included in the Cassini distribution image, they may be
run via the ptest framework, using the following command after booting the
image and logging in:

.. code-block:: console

   ptest-runner [test-suite-id]

If the test suite identifier (``[test-suite-id]``) is omitted, all integration
tests will be run.  For example, running ``ptest-runner`` produces output such
as the following:

.. code-block:: console

   $ ptest-runner
   START: ptest-runner
   [...]
   PASS:container-engine-integration-tests
   [...]
   PASS:k3s-integration-tests
   [...]
   PASS:user-accounts-integration-tests
   [...]
   STOP: ptest-runner

.. note::
  ``ptest-runner -l`` is a useful command to list the available test suites in
  the image.

Alternatively, a single standalone test suite may be run via a runner script
included in the test suite directory:

.. code-block:: console

   /usr/share/[test-suite-id]/run-[test-suite-id]

Upon completion of the test-suite, a result indicator will be output by the
script, as one of two options: ``PASS:[test-suite-id]`` or
``FAIL:[test-suite-id]``, as well as an appropriate exit status.

A test suite consists of one or more 'top-level' BATS tests, which may be
composed of multiple assertions, where each assertion is considered a named
sub-test. If a sub-test fails, its individual result will be included in the
output with a similar format. In addition, if a test failed then debugging
information will be provided in the output of type ``DEBUG``. The format of
these results are described in :ref:`test_logging_label`.

.. _test_logging_label:

Test Logging
============

Test suite execution outputs results and debugging information into a log file.
As the test suites are executed using the ``test`` user account, this log file
will be owned by the ``test`` user and located in the ``test`` user's home
directory by default, at:

    ``/home/test/runtime-integration-tests-logs/[test-suite-id].log``

Therefore, reading this file as another user will require ``sudo`` access. The
location of the log file for each test suite is customizable, as described in
the detailed documentation for each test suite below. The log file is replaced
on each new execution of a test suite.

The log file will record the results of each top-level integration test, as
well as a result for each individual sub-test up until a failing sub-test is
encountered.

Each top-level result is formatted as:

    ``TIMESTAMP RESULT:[top_level_test_name]``

Each sub-test result is formatted as:

    ``TIMESTAMP RESULT:[top_level_test_name]:[sub_test_name]``

Where ``TIMESTAMP`` is of the format ``%Y-%m-%d %H:%M:%S`` (see
|Python Datetime Format Codes|_), and ``RESULT`` is either ``PASS``, ``FAIL``,
or ``SKIP``.

On a test failure, a debugging message of type ``DEBUG`` will be written to
the log. The format of a debugging message is:

    ``TIMESTAMP DEBUG:[top_level_test_name]:[return_code]:[stdout]:[stderr]``

Additional informational messages may appear in the log file with ``INFO`` or
``DEBUG`` message types, e.g. to log that an environment clean-up action
occurred.

Test Suites
===========

The test suites are detailed below.

Container Engine Tests
----------------------

The container engine test suite is identified as:

    ``container-engine-integration-tests``

for execution via ``ptest-runner`` or as a standalone BATS suite, as described
in :ref:`running_the_tests_label`.

The test suite is built and installed in the image according to the following
BitBake recipe:
``meta-cassini-tests/recipes-tests/runtime-integration-tests/container-engine-integration-tests.bb``.

Currently the test suite contains three top-level integration tests, which run
consecutively in the following order.

| 1. ``run container`` is composed of four sub-tests:
|    1.1. Run a containerized detached workload via the ``docker run`` command
|        - Pull an image from the network
|        - Create and start a container
|    1.2. Check the container is running via the ``docker inspect`` command
|    1.3. Remove the running container via the ``docker remove`` command
|        - Stop the container
|        - Remove the container from the container list
|    1.4. Check the container is not found via the ``docker inspect`` command
| 2. ``container network connectivity`` is composed of a single sub-test:
|    2.1. Run a containerized, immediate (non-detached) network-based workload
         via the ``docker run`` command
|        - Create and start a container, re-using the existing image
|        - Update package lists within container from external network

The tests can be customized via environment variables passed to the execution,
each prefixed by ``CE_`` to identify the variable as associated to the
container engine tests:

|  ``CE_TEST_IMAGE``: defines the container image
|    Default: ``docker.io/library/alpine``
|  ``CE_TEST_LOG_DIR``: defines the location of the log file
|    Default: ``/home/test/runtime-integration-tests-logs/``
|    Directory will be created if it does not exist
|    See :ref:`test_logging_label`
|  ``CE_TEST_CLEAN_ENV``: enable test environment clean-up
|    Default: ``1`` (enabled)
|    See :ref:`container_engine_environment_clean-up_lable`

.. _container_engine_environment_clean-up_lable:

Container Engine Environment Clean-Up
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A clean environment is expected when running the container engine tests. For
example, if the target image already exists within the container engine
environment, then the functionality to pull the image over the network will not
be validated. Or, if there are running containers from previous (failed) tests
then they may interfere with subsequent test executions.

Therefore, if ``CE_TEST_CLEAN_ENV`` is set to ``1`` (as is default), running
the test suite will perform an environment clean before and after the suite
execution.

The environment clean operation involves:

    * Determination and removal of all running containers of the image given by
      ``CE_TEST_IMAGE``
    * Removal of the image given by ``CE_TEST_IMAGE``, if it exists

If enabled then the environment clean operations will always be run, regardless
of test-suite success or failure.

K3s Orchestration Tests
-----------------------

The K3s test suite is identified as:

    ``k3s-integration-tests``

for execution via ``ptest-runner`` or as a standalone BATS suite, as described
in :ref:`running_the_tests_label`.

The test suite is built and installed in the image according to the following
BitBake recipe within
``meta-cassini-tests/recipes-tests/runtime-integration-tests/k3s-integration-tests.bb``.

Currently the test suite contains a single top-level integration test which
validates the deployment and high-availability of a test workload based on the
|Nginx|_ webserver.

The K3s integration tests consider a single-node cluster, which runs a K3s
server together with its built-in worker agent. The containerized test workload
is therefore deployed to this node for scheduling and execution.

The test suite will not be run until the appropriate K3s services are in the
'active' state, and all 'kube-system' pods are either running, or have
completed their workload.

| 1. ``K3s container orchestration`` is composed of many sub-tests, grouped here
     by test area:
|    **Workload Deployment:**
|    1.1. Deploy test Nginx workload from YAML file via ``kubectl apply``
|    1.2. Ensure Pods are initialized via ``kubectl wait``
|    1.3. Create NodePort Service to expose Deployment via
          ``kubectl create service``
|    1.4. Get the IP of the node(s) running the Deployment via ``kubectl get``
|    1.5. Ensure web service is accessible on the node(s) via ``wget``
|    **Deployment Upgrade:**
|    1.6. Check initial image version of running Deployment via ``kubectl get``
|    1.7. Get all pre-upgrade Pod names running Deployment via ``kubectl get``
|    1.8. Upgrade image version of Deployment via ``kubectl set``
|    1.9. Ensure a new set of Pod names have been started via ``kubectl wait``
     and ``kubectl get``
|    1.10. Check Pods are running the upgraded image version via ``kubectl get``
|    1.11. Ensure web service is still accessible on the node(s) via ``wget``
|    **Server Failure Tolerance:**
|    1.12. Stop K3s server Systemd service via ``systemctl stop``
|    1.13. Ensure web service is still accessible on the node(s) via ``wget``
|    1.14. Restart the Systemd service via ``systemctl start``
|    1.15. Check K3S server is again responding to ``kubectl get``

The tests can be customized via environment variables passed to the execution,
each prefixed by ``K3S_`` to identify the variable as associated to the
K3s orchestration tests:

|  ``K3S_TEST_LOG_DIR``: defines the location of the log file
|    Default: ``/home/test/runtime-integration-tests-logs/``
|    Directory will be created if it does not exist
|    See :ref:`test_logging_label`
|  ``K3S_TEST_CLEAN_ENV``: enable test environment clean-up
|    Default: ``1`` (enabled)
|    See :ref:`k3s_environment_clean-up_label`

.. _k3s_environment_clean-up_label:

K3s Environment Clean-Up
^^^^^^^^^^^^^^^^^^^^^^^^

A clean environment is expected when running the K3s integration tests, to
ensure that the system is ready to be validated. For example, the test suite
expects that the Pods created from any previous execution of the integration
tests have been deleted, in order to test that a new Deployment successfully
initializes new Pods for orchestration.

Therefore, if ``K3S_TEST_CLEAN_ENV`` is set to ``1`` (as is default), running
the test suite will perform an environment clean before and after the suite
execution.

The environment clean operation involves:

    * Deleting any previous K3s test Service
    * Deleting any previous K3s test Deployment, ensuring corresponding Pods
      are also deleted

If enabled then the environment clean operations will always be run, regardless
of test-suite success or failure.

.. _user_accounts_tests_label:

User Accounts Tests
-------------------

The User Accounts test suite is identified as:

    ``user-accounts-integration-tests``

for execution via ``ptest-runner`` or as a standalone BATS suite, as described
in :ref:`running_the_tests_label`.

The test suite is built and installed in the image according to the following
BitBake recipe within
``meta-cassini-tests/recipes-tests/runtime-integration-tests/user-accounts-integration-tests.bb``.

The test suite validates that the user accounts described in
:ref:`User Accounts<developer_manual/user_accounts:User Accounts>` are
correctly configured with appropriate access permissions on the Cassini
distribution image. The validation performed by the test suite is dependent
whether or not it has been configured with
:ref:`Cassini Security Hardening<developer_manual/security_hardening:Security Hardening>`.

As the configuration of user accounts is modified for Cassini distribution
image which is built with Cassini security hardening, additional
security-related validation is included in the test suite for this image. These
additional tests validate that the appropriate password requirements and that
the mask configuration for permission control of newly created files and
directories is applied correctly.

The test suite therefore contains following integration tests:

| 1. ``user accounts management tests`` is composed of three sub-tests:
|    1.1. Check home directory permissions are correct for the default
          non-privileged Cassini user account, via the filesystem ``stat``
          utility
|    1.2. Check the default privileged Cassini user account has ``sudo``
          command access
|    1.3. Check the default non-privileged Cassini user account does not have
          ``sudo`` command access
| 2. ``user accounts management additional security tests`` is only included
     for images configured with Cassini security hardening, and is composed of
     four sub-tests:
|    2.1. Log-in to a local console using the non-privileged Cassini user
          account
|        - As part of the log-in procedure, validate the user is prompted to
           set an account password
|    2.2. Check that the umask value is set correctly

The tests can be customized via environment variables passed to the execution,
each prefixed by ``UA_`` to identify the variable as associated to the user
accounts tests:

|  ``UA_TEST_LOG_DIR``: defines the location of the log file
|    Default: ``/home/test/runtime-integration-tests-logs/``
|    Directory will be created if it does not exist
|    See :ref:`test_logging_label`
|  ``UA_TEST_CLEAN_ENV``: enable test environment clean-up
|    Default: ``1`` (enabled)
|    See :ref:`user_accounts_environment_clean-up_label`

.. _user_accounts_environment_clean-up_label:

User Accounts Environment Clean-Up
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As the user accounts integration tests only modify the system for images built
with Cassini security hardening, clean-up operations are only performed when
running the test suite on these images.

In addition, the clean-up operations will only occur if ``UA_TEST_CLEAN_ENV`` is
set to ``1`` (as is default).

The environment clean-up operations for images built with Cassini security
hardening are:

    * Reset the password for the ``test`` user account
    * Reset the password for the non-privileged Cassini user account

After the environment clean-up, the user accounts will return to their original
state where the first log-in will prompt the user for a new account password.

If enabled then the environment clean operations will always be run, regardless
of test-suite success or failure.

Parsec simple end2end Tests
---------------------------

The Parsec simple end2end test suite is identified as:

    ``parsec-simple-e2e-tests``

for execution via ``ptest-runner`` or as a standalone BATS suite, as described
in :ref:`running_the_tests_label`.

The test suite is built and installed in the image according to the following
BitBake recipe within
``meta-cassini-tests/recipes-tests/runtime-integration-tests/parsec-simple-e2e-tests.bb``.

The test suite validates Parsec service in Cassini distribution image by
running simple end2end tests available in `parsec-tool <https://github.com/parallaxsecond/parsec-tool/blob/main/tests/parsec-cli-tests.sh>`_.

The tests can be customized via environment variables passed to the execution,
each prefixed by ``PS_`` to identify the variable as associated to the Parsec
simple end2end tests:

|  ``PS_TEST_LOG_DIR``: defines the location of the log file
|    Default: ``/home/test/runtime-integration-tests-logs/``
|    Directory will be created if it does not exist
|    See :ref:`test_logging_label`
|  ``PS_TEST_CLEAN_ENV``: enable test environment clean-up
|    Default: ``1`` (enabled)
|    See :ref:`parsec_simple_end2end_tests_environment_clean-up_label`

.. _parsec_simple_end2end_tests_environment_clean-up_label:

Parsec Simple End2End Tests Environment Clean-Up
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In addition, the clean-up operations will only occur if ``PS_TEST_CLEAN_ENV``
is set to ``1`` (as is default).

Currently, no clean-up is required as simple end2end tests script
``parsec-cli-tests.sh`` cleans up temporary files before exiting.

If enabled then the environment clean operations will always be run, regardless
of test-suite success or failure.
