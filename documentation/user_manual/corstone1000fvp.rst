..
 # SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

##########################################
Getting Started with Arm Corstone-1000 FVP
##########################################

This document explains how to build and boot the Cassini distro on the Arm
Corstone-1000 FVP.

*****
Build
*****

The provided kas configuration file meta-cassini-config/kas/corstone1000-fvp.yml can be used to build images
that are targeting the Corstone-1000 FVP.

.. note::
  To build and run any image for the Corstone-1000 FVP the user has to
  accept its |EULA|_, which can be done by executing
  the following command in the build environment:

  .. code-block:: console

    export FVP_CORSTONE1000_EULA_ACCEPT=True

*******************
Building FVP images
*******************

To build Corstone-1000 FVP images:

  .. code-block:: console

    kas build --update meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

Or if using kas-container:

  .. code-block:: console

    kas-container --runtime-args "-e FVP_CORSTONE1000_EULA_ACCEPT=True" build \
    meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

This will produce a Corstone-1000 firmware image here:
  ``build/tmp-firmware/deploy/images/corstone1000-fvp/corstone1000-image-corstone1000-fvp.wic.nopt``

And a Cassini distribution image here:
  ``build/tmp/deploy/images/corstone1000-fvp/cassini-image-base-corstone1000-fvp.wic``

***************
Running the FVP
***************

To start the FVP and get the console:

  .. code-block:: console

    kas shell -c "../layers/meta-arm/scripts/runfvp --verbose --console" \
    meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

Or if using kas-container:

  .. code-block:: console

    kas-container --runtime-args "-e FVP_CORSTONE1000_EULA_ACCEPT=True" \
    shell -c "/work/layers/meta-arm/scripts/runfvp --verbose --console" \
    meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

.. _reproduce_run-time_integration_tests:

**********
Validation
**********

The following validation tests can be performed on the Cassini Reference Stack:

  * System Integration Tests:

    * Cassini Architecture Stack:

      .. code-block:: console

        TESTIMAGE_AUTO=1 kas build meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-fvp.yml

      The previous test takes around 2 minutes to complete.

      A similar output should be printed out:

      .. code-block:: console

        NOTE: Executing Tasks
        Creating terminal default on host_terminal_0
        default: Waiting for login prompt
        RESULTS:
        RESULTS - linuxboot.LinuxBootTest.test_linux_boot: PASSED (23.70s)
        SUMMARY:
        cassini-image-base () - Ran 1 test in 23.704s
        cassini-image-base - OK - All required tests passed (successes=1, skipped=0, failures=0, errors=0)
