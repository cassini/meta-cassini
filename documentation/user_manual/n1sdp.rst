..
 # SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

##############################
Getting Started with the N1SDP
##############################

This document explains how to build, deploy, and boot the Cassini distro on the
Arm Neoverse N1 System Development Platform (N1SDP).

**NOTE:** Requires a micro SD card (2 GB) and a USB drive (at least 16 GB)

*********************
Building N1SDP images
*********************

The kas configuration file ``meta-cassini-config/kas/n1sdp.yml`` can be used to
build images which target the N1SDP. To build N1SDP images:

.. code-block:: console

  kas build --update meta-cassini-config/kas/cassini.yml:meta-cassini-config kas/n1sdp.yml

This will produce an N1SDP firmware image here:
  ``build/tmp/deploy/images/n1sdp/n1sdp-board-firmware_primary.tar.gz``

And a Cassini distribution image here:
  ``build/tmp/deploy/images/n1sdp/cassini-image-base-n1sdp.wic.gz``
  ``build/tmp/deploy/images/n1sdp/cassini-image-base-n1sdp.wic.bmap``

***********************
Connecting to the N1SDP
***********************

1. Connect a USB cable between the build host and the ``DBG USB`` port on the N1SDP back panel and power on the device

2. Check four new TTY USB devices are seen by the build host, via:

  .. code-block:: shell

    ls /dev/ttyUSB*

  This will output, for example:

  .. code-block:: console

    /dev/ttyUSB0
    /dev/ttyUSB1
    /dev/ttyUSB2
    /dev/ttyUSB3

  If there are no other TTY USB devices, then the four ports on the N1SDP will be
  connected as follows:

    * ttyUSB0 Motherboard Configuration Controller (MCC)

    * ttyUSB1 Application processor (AP)

    * ttyUSB2 System Control Processor (SCP)

    * ttyUSB3 Manageability Control Processor (MCP) (or OP-TEE and Secure Partitions)

  The rest of this guide assumes there are no other TTY USB devices on the build host

3. Connect to the serial console(s) using any terminal client (``picocom``, ``minicom``, or ``screen`` should all work).

  All ports are configured with:

    * 115200 Baud

    * 8 bits, No parity, 1 stop bit (8N1)

    * No hardware or software flow control

  For example, run the following command to open a new picocom session for
  the AP console:

  .. code-block:: shell

    sudo picocom -b 115200 /dev/ttyUSB1

  .. note::

    ``sudo`` should not be required if the current user is in the ``dialout`` group

******************************************
Updating the MCC firmware (Micro SD image)
******************************************

1. Follow the instructions above and connect to the MCC console i.e.

  .. code-block:: shell

    sudo picocom -b 115200 /dev/ttyUSB0

2. In the MCC console, at the ``Cmd>`` prompt, type the following command to see MCC firmware version and a list of commands:

  .. code-block:: console

    ?

  This will output, for example:

  .. code-block:: console

    Arm N1SDP MCC Firmware v1.0.1
    Build Date: Sep  5 2019
    Build Time: 14:18:16
    + command ------------------+ function ---------------------------------+
    | CAP "fname" [/A]          | captures serial data to a file            |
    |                           |  [/A option appends data to a file]       |
    | FILL "fname" [nnnn]       | create a file filled with text            |
    |                           |  [nnnn - number of lines, default=1000]   |
    | TYPE "fname"              | displays the content of a text file       |
    | REN "fname1" "fname2"     | renames a file 'fname1' to 'fname2'       |
    | COPY "fin" ["fin2"] "fout"| copies a file 'fin' to 'fout' file        |
    |                           |  ['fin2' option merges 'fin' and 'fin2']  |
    | DEL "fname"               | deletes a file                            |
    | DIR "[mask]"              | displays a list of files in the directory |
    | FORMAT [label]            | formats Flash Memory Card                 |
    | USB_ON                    | Enable usb                                |
    | USB_OFF                   | Disable usb                               |
    | SHUTDOWN                  | Shutdown PSU (leave micro running)        |
    | REBOOT                    | Power cycle system and reboot             |
    | RESET                     | Reset Board using CB_nRST                 |
    | DEBUG                     | Enters debug menu                         |
    | EEPROM                    | Enters eeprom menu                        |
    | HELP  or  ?               | displays this help                        |
    |                                                                       |
    | THE FOLLOWING COMMANDS ARE ONLY AVAILABLE IN RUN MODE                 |
    |                                                                       |
    | CASE_FAN_SPEED "SPEED"    | Choose from SLOW, MEDIUM, FAST            |
    | READ_AXI "fname"          | Read system memory to file 'fname'        |
    |          "address"        | from address to end address               |
    |          "end_address"    |                                           |
    | WRITE_AXI "fname"         | Write file 'fname' to system memory       |
    |           "address"       | at address                                |
    +---------------------------+-------------------------------------------+

3. Type the following command to enable USB:

  .. code-block:: console

    USB_ON

4. Check a new block device is seen by the build host, via:

  .. code-block:: shell

    lsblk

  This will output, for example:

  .. code-block:: console

    NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    sdb      8:0    0     2G  0 disk
    └─sdb1   8:1    0     2G  0 part

  If there are no similar block devices mounted on the build host, then it
  should be possible to identify the Micro SD Card on the N1SDP by its' size.
  In the output above, the Micro SD partition is clearly ``sdb1``.

5. Mount the device and check its contents:

  .. code-block:: console

    sudo umount /dev/sdb1 &&
    sudo mkdir -p /tmp/sdcard &&
    sudo mount /dev/sdb1 /tmp/sdcard &&
    ls -l /tmp/sdcard

  This should output, for example:

  .. code-block:: console

    config.txt  ee0316a.txt  LIB  LICENSES  LOG.TXT  MB

  .. warning::

    In this example, the ``/dev/sdb1`` partition is being mounted. As this may
    vary on different machines, care should be taken when copying and pasting
    the following commands. Don't proceed unless the contents of the Micro SD
    Card were as expected in the previous step.

6. Wipe the mounted microSD card, then extract the contents of ``n1sdp-board-firmware_primary.tar.gz`` onto it:

  .. code-block:: console

    sudo rm -rf /tmp/sdcard/* &&
    sudo tar --no-same-owner -xf build/tmp/deploy/images/n1sdp/n1sdp-board-firmware_primary.tar.gz -C /tmp/sdcard/ &&
    sudo sync

  .. note::

    If the N1SDP board was manufactured after November 2019 (Serial Number
    greater than ``36253xxx``), a different PMIC firmware image must be used to
    prevent potential damage to the board. More details can be found in
    `Potential firmware damage notice`_. The ``MB/HBI0316A/io_v123f.txt`` file
    located in the microSD needs to be updated. To update it, set the PMIC image
    (``300k_8c2.bin``) to be used in the newer models by running the following
    commands on the Build Host:

  .. code-block:: console

    sudo sed -i '/^MBPMIC: pms_0V85.bin/s/^/;/g' /tmp/sdcard/MB/HBI0316A/io_v123f.txt
    sudo sed -i '/^;MBPMIC: 300k_8c2.bin/s/^;//g' /tmp/sdcard/MB/HBI0316A/io_v123f.txt
    sudo sync

7. Unmount the device

  .. code-block:: console

    sudo umount /tmp/sdcard
    sudo rmdir /tmp/sdcard

**************************************************
Prepare the distro image for the N1SDP (USB image)
**************************************************

1. Insert the USB storage device into the build host

2. Check a new block device is seen by the build host, via:

  .. code-block:: shell

    lsblk

  This will output, for example:

  .. code-block:: console

    NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
    sdb      8:0    0     2G  0 disk
    └─sdb1   8:1    0     2G  0 part
    sdc      8:0    0    64G  0 disk

  If there are no similar block devices mounted on the build host, then it
  should be possible to identify the USB storage device by its' size. In the
  output above, the USB storage device is ``sdc``.

  .. warning::

    The next step will result in all prior partitions and data on the USB
    storage device being erased. Take care not to confuse your host PC's own
    hard drive with the USB drive and backup any data on the USB storage device
    before continuing.

3. Flash the image onto the USB storage device using ``bmap-tools``:

  .. code-block:: shell

    sudo bmaptool copy --bmap cassini-image-base-n1sdp.wic.bmap cassini-image-base-n1sdp.wic.gz /dev/<usb drive>

  Or if deploying an SDK image

  .. code-block:: console

    sudo bmaptool copy --bmap cassini-image-sdk-n1sdp.wic.bmap cassini-image-sdk-n1sdp.wic.gz /dev/<usb drive>

4. Eject the USB storage device from the build host and plug it into one of the USB 3.0 ports on the N1SDP

5. Reboot the N1SDP device by power cycling it or typing the following at the MCC console

  .. code-block:: console

    REBOOT
