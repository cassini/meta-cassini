..
 # SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited and/or its
 # affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: MIT

###############################################
Getting Started with Arm Corstone-1000 for MPS3
###############################################

This document explains how to build, deploy, and boot the Cassini distro on the
Arm Corstone-1000 for MPS3.

**NOTE:** Requires a micro SD card (at least 4 GB) and a USB drive (at
least 16 GB)

*****
Build
*****

The kas configuration file ``meta-cassini-config/kas/corstone1000-mps3.yml``
can be used to build images which target the Corstone-1000 for MPS3.

********************
Building MPS3 images
********************

To build Corstone-1000 MPS3 images:

  .. code-block:: console

    kas build --update meta-cassini-config/kas/cassini.yml:meta-cassini-config/kas/corstone1000-mps3.yml

This will produce a Corstone-1000 firmware image here:
  ``build/tmp-firmware/deploy/images/corstone1000-mps3/corstone1000-image-corstone1000-mps3.wic.nopt``

And a Cassini distribution image here:
  ``build/tmp/deploy/images/corstone1000-mps3/cassini-image-base-corstone1000-mps3.wic``
  ``build/tmp/deploy/images/corstone1000-mps3/cassini-image-base-corstone1000-mps3.wic.bmap``

***************************************************
Prepare the firmware image for FPGA (Micro SD card)
***************************************************

The user should download the FPGA bit file image from `this link <https://developer.arm.com/tools-and-software/development-boards/fpga-prototyping-boards/download-fpga-images>`__
and under the section ``AN550: Arm® Corstone™-1000 for MPS3``.

Only copy the current directory structure shown below on to the Micro SD Card.

.. code-block:: console

    MB
    ├── BRD_LOG.TXT
    ├── HBI0309B
    │   ├── AN550
    │   │   ├── AN550_v1.bit
    │   │   ├── an550_v1.txt
    │   │   └── images.txt
    │   ├── board.txt
    │   └── mbb_v210.ebf
    └── HBI0309C
        ├── AN550
        │   ├── AN550_v1.bit
        │   ├── an550_v1.txt
        │   └── images.txt
        ├── board.txt
        └── mbb_v210.ebf
    SOFTWARE
    ├── ES0.bin
    ├── SE.bin
    └── an550_st.axf
    config.txt


Depending upon the MPS3 board version (printed on the MPS3 board HBI0309B or HBI0309C) you should
update the ``./AN550/images.txt`` file so that the file points to the images under SOFTWARE directory.

Here is an example

.. code-block:: console

  ;************************************************
  ;       Preload port mapping                    *
  ;************************************************
  ;  PORT 0 & ADDRESS: 0x00_0000_0000 QSPI Flash (XNVM) (32MB)
  ;  PORT 0 & ADDRESS: 0x00_8000_0000 OCVM (DDR4 2GB)
  ;  PORT 1        Secure Enclave (M0+) ROM (64KB)
  ;  PORT 2        External System 0 (M3) Code RAM (256KB)
  ;  PORT 3        Secure Enclave OTP memory (8KB)
  ;  PORT 4        CVM (4MB)
  ;************************************************

  [IMAGES]
  TOTALIMAGES: 2      ;Number of Images (Max: 32)

  IMAGE0PORT: 1
  IMAGE0ADDRESS: 0x00_0000_0000
  IMAGE0UPDATE: RAM
  IMAGE0FILE: \SOFTWARE\bl1.bin

  IMAGE1PORT: 0
  IMAGE1ADDRESS: 0x00_00010_0000
  IMAGE1UPDATE: AUTOQSPI
  IMAGE1FILE: \SOFTWARE\cs1000.bin

The binaries are present in OUTPUT_DIR = ``<_workspace>/build/tmp/deploy/images/corstone1000-mps3`` directory.

1. Copy ``bl1.bin`` from OUTPUT_DIR to SOFTWARE directory of the Micro SD card.
2. Copy ``corstone1000-image-corstone1000-mps3.wic`` from OUTPUT_DIR directory to SOFTWARE
   directory of the Micro SD card and rename the wic image to ``cs1000.bin``.

**NOTE:** Renaming of the images are required because MCC firmware has
limitation of 8 characters before .(dot) and 3 characters after .(dot).

*********************************************
Prepare the distro image for FPGA (USB image)
*********************************************

Use the ``lsblk`` command to determine USB drive and bmap tool to copy the cassini distro to it.

.. code-block:: console

  lsblk
  sudo bmaptool copy --bmap cassini-image-base-corstone1000-mps3.wic.bmap cassini-image-base-corstone1000-mps3.wic /dev/<usb drive>


****************************
Running the software on FPGA
****************************

Insert SD card and USB drive before switching ON the device.

On the host machine, connect the board via USB.

If there are no other TTY USB devices, then the three ports from the MPS3
will be connected as follows:

  - ttyUSB0 for MCC, OP-TEE and Secure Partition
  - ttyUSB1 for Boot Processor (Cortex-M0+)
  - ttyUSB2 for Host Processor (Cortex-A35)

The rest of this guide assumes there are no other TTY USB devices on the
host machine.

Connect to the serial console(s) using any terminal client (``picocom``,
``minicom``, or ``screen`` should all work).

For example, run the following commands to open new picocom sessions for
each port:

.. code-block:: console

  sudo picocom -b 115200 /dev/ttyUSB0
  sudo picocom -b 115200 /dev/ttyUSB1
  sudo picocom -b 115200 /dev/ttyUSB2

.. note::

  ``sudo`` should not be required if the current user is in the
  ``dialout`` group
