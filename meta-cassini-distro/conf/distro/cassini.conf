# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

require ${COREBASE}/meta-poky/conf/distro/poky.conf

include conf/machine/include/${MACHINE}-cassini-extra-settings.inc

# Unix group name for dev/tee* ownership.
TEE_GROUP_NAME = "teeclnt"

# Introduce the CASSINI Yocto Distro
DISTRO = "cassini"
DISTRO_NAME = "CASSINI"
DISTRO_VERSION = "unstable"

# systemd as the init system
INIT_MANAGER = "systemd"
DISTRO_FEATURES = "largefile usbhost ipv4 virtualization seccomp k3s cassini-parsec"

# Apply feature specific config
USER_CLASSES:append = " cassini-distro-features"

# rpm as the package management system
PACKAGE_CLASSES ?= "package_rpm"

PREFERRED_PROVIDER_virtual/runc ?= "runc-opencontainers"
PREFERRED_PROVIDER_virtual/docker ?= "docker-ce"

# These classes produce warnings if there are any missing kernel configurations
# that are required by their target packages
KERNEL_CLASSES:append = " containers_kernelcfg_check k3s_kernelcfg_check"

# This variable is used to include cassini distro feature specific settings
CASSINI_OVERRIDES ??= ""

# Default user account names and default admin group name
CASSINI_USER_ACCOUNT ?= "user"
CASSINI_ADMIN_ACCOUNT ?= "cassini"
CASSINI_TEST_ACCOUNT ?= "test"
CASSINI_ADMIN_GROUP ?= "sudo"
CASSINI_ADMIN_GROUP_OPTIONS ?= "NOPASSWD:"

# Select whether or not to generate the filesystem able to run in any aarch64
# platform. If CASSINI_GENERIC_ARM64_FILESYSTEM is "1", armv8a-crc DEFAULTTUNE
# will be used otherwise the one set by the MACHINE conf will be selected.
CASSINI_GENERIC_ARM64_FILESYSTEM ?= "1"
CASSINI_GENERIC_ARM64_DEFAULTTUNE ?= "armv8a-crc"

# Use generic DEFAULTTUNE if generic filestystem enabled otherwise preserve
# DEFAULTTUNE value.
DEFAULTTUNE:aarch64 := "\
${@ d.getVar('CASSINI_GENERIC_ARM64_DEFAULTTUNE', False) \
    if (bb.utils.to_boolean(d.getVar('CASSINI_GENERIC_ARM64_FILESYSTEM', \
                                     True), False)) \
    else d.getVar('DEFAULTTUNE', False)}"
