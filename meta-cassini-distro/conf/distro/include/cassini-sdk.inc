# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

# Config specific to the cassini-sdk distro feature, enabled using
# DISTRO_FEATURES

include cassini-dev.inc

IMAGE_FEATURES:append:libc-glibc = " \
 package-management \
 dev-pkgs \
 tools-sdk \
 tools-debug \
 tools-profile \
 ssh-server-openssh"

IMAGE_INSTALL:append:libc-glibc = " kernel-base kernel-devsrc kernel-modules"

IMAGE_INSTALL:append:aarch64:libc-glibc = " gator-daemon"
