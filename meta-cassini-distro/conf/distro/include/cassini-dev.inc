# Copyright (c) 2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

IMAGE_FEATURES:append = " debug-tweaks"
