# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

# Config specific to the cassini-test distro feature, enabled using
# DISTRO_FEATURES

include cassini-dev.inc

DISTRO_FEATURES:append:libc-glibc = " ptest"

IMAGE_INSTALL:append:libc-glibc = " jfrog-cli \
                                packagegroup-ts-tests-psa \
                                psa-arch-tests-ptest \
                                container-engine-integration-tests-ptest \
                                k3s-integration-tests-ptest \
                                user-accounts-integration-tests-ptest \
                                ${@bb.utils.contains('DISTRO_FEATURES',\
                                'cassini-parsec', 'parsec-simple-e2e-tests-ptest', '', d)} \
                                "

IMAGE_INSTALL:append:n1sdp = " optee-test optee-xtests-ptest"

EXTRA_USERS_PARAMS:prepend:libc-glibc = "usermod -aG teeclnt ${CASSINI_TEST_ACCOUNT};"
