# Copyright (c) 2022-2023 Arm Limited and/or its affiliates.
# <open-source-office@arm.com>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:libc-glibc:cassini := "${THISDIR}/files:"

SRC_URI:append:libc-glibc:cassini = " file://cassini_admin_group.in"

DEPENDS:append:libc-glibc:cassini = " gettext-native"

do_install:append:libc-glibc:cassini() {

    export ADMIN_GROUP="${CASSINI_ADMIN_GROUP}"
    export ADMIN_GROUP_OPTIONS="${CASSINI_ADMIN_GROUP_OPTIONS}"

    envsubst < ${WORKDIR}/cassini_admin_group.in > ${D}${sysconfdir}/sudoers.d/cassini_admin_group
    chmod 644 ${D}${sysconfdir}/sudoers.d/cassini_admin_group
}
